const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const User = require('../models/user')
const bodyParser = require('body-parser')
const bcrypt = require('bcryptjs')
const jwt = require("jsonwebtoken");
const { check, validationResult } = require('express-validator');
const auth = require('../middleware/auth')

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', auth, async (req, res) => {
  try {
    const users = await User.find()
    res.send(users)
  } catch (error) {
    res.json({ message: error })
  }
})

router.post('/', [
  check('email').isEmail().withMessage('enter valid email format'),
  check('password').isLength({ min: 6 }).withMessage('must be at least 6 chars long')],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {

      const salt = await bcrypt.genSalt(10);
      const user = await User({
        name: req.body.name,
        email: req.body.email,
        password: await bcrypt.hash(req.body.password, salt)
      })
      await user.save()
      res.send(user)
    } catch (error) {
      console.log(error);

      res.json({ message: error })
    }
  })

router.post('/login', async (req, res) => {
  try {
    const user = await User.findOne({
      email: req.body.email,
    })
    if (!user) {
      return res.status(404).json({ message: 'user not exists' })
    }
    const isMatch = await bcrypt.compare(req.body.password, user.password)

    if (!isMatch)
      return res.status(400).json({
        message: "Incorrect Password !"
      });

    const payload = { user: { id: user.id, email: user.email } }
    jwt.sign(payload, "randomString", { expiresIn: 3600 }, (err, token) => {
      if (err) throw err
      res.status(200).json({ token })
    })
  } catch (error) {
    console.log(error)
    res.json({ message: error })
  }
})

router.post('/:userId', async (req, res) => {
  try {
    const updateUser = await User.update({ _id: req.params.userId }, { $set: { name: req.body.name, password: req.body.password } })
    res.send(updateUser)
  } catch (error) {
    res.send(error)
  }
})

router.post('/:userId', async (req, res) => {
  try {
    const deleteUser = await User.remove({ _id: req.params.userId })
    res.send(deleteUser)
  } catch (error) {
    res.send(error)
  }
})
module.exports = router