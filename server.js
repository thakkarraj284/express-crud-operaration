const express = require('express')
const app = express()
const router = express.Router()
const bodyParser = require('body-parser')
const userRoute = require('./routes/user')
require('./models/db')
app.use('/users', userRoute)

app.get('/', (req, res) => {
  res.send('hello worlds')
})

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

app.listen(3000, () => { console.log('listening on 3000') })